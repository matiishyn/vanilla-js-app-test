module.exports = function (grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        jshint: {
            files: ['app/js/*.js'],
            options: {
                globals: {
                    console: true
                }
            }
        },

        watch: {
            files: ['<%= jshint.files %>'],
            tasks: []
        }
    });

    grunt.loadNpmTasks('grunt-contrib-jshint');

    grunt.registerTask('default', ['jshint']);
};