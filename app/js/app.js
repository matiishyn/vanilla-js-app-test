(function () {
    'use strict';

    var htmlGallery = new G.FlickrGallery("html");
    htmlGallery.getImages();

    var cssGallery = new G.FlickrGallery("css");
    cssGallery.getImages();
})();