(function () {
    'use strict';

    var View = function (configs) {
        this.el = configs.el || 'body';
        this.$el = G.helpers.qs(this.el);
        this.data = configs.data || {};
        this.template = configs.template;
        this.className = configs.className || '';
    };

    View.prototype.render = function (ifAppend) {
        ifAppend ?
            this.$el.innerHTML += G.helpers.template(this.template, this.data) :
            this.$el.innerHTML = G.helpers.template(this.template, this.data);
        if(this.className){
            this.$el.firstElementChild.classList.add(this.className)
        }
    };

    /*View.prototype.addClass = function (className) {
        this.$el.classList.add(className);
    };*/


    window.G = window.G || {};
    window.G.View = View;
})(window);