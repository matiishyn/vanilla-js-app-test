/* jshint expr:true */
(function (window) {
    'use strict';
    var G = {}; // Gallery application main object
    G.helpers = {
        // SELECTORS
        i: function (id, scope) {
            return (scope || document).getElementById(id);
        },
        qs: function (selector, scope) {
            return (scope || document).querySelector(selector);
        },
        // AJAX
        ajax: function () {
            var configs = {},
                url,
                callback,
                prop;
            switch (typeof arguments[0]) {
                case 'function':
                    callback = arguments[0];
                    break;
                case 'object':
                    // first parameter is configs object
                    // second callback
                    configs = arguments[0];
                    callback = arguments[1];
                    break;
                case 'string':
                    // first parameter is url string
                    // second callback
                    url = arguments[0];
                    callback = arguments[1];
                    break;
                default:
                    console.error('different params for ajax');
            }

            prop = {
                url: configs.url || url || this.url,
                method: configs.method || 'GET',
                async: (configs.async === false) ? false : true,
                dataType: configs.dataType || 'json',
                jsonp: configs.jsonp || 'callback',  // name of callback function
                data: configs.data || {}
            };

            if (prop.dataType === 'jsonp') {
                this._jsonpRequest(prop, callback);
            } else {
                // simple ajax
                this._ajaxRequest(prop, callback);
            }
        },

        _ajaxRequest: function (prop, callback) {

            var xhr = new XMLHttpRequest();
            xhr.open(prop.method, prop.url, prop.async);
            xhr.send();

            if (!prop.async) {
                // SYNC AJAX
                callback(xhr.responseText);
            } else {
                // ASYNC AJAX
                xhr.onreadystatechange = function () {
                    if (xhr.readyState === 4) {
                        if (xhr.status === 200 || xhr.status === 304) {
                            !callback || callback(xhr.responseText);
                        }
                    }
                };
            }
        },

        _jsonpRequest: function (prop, callback) {
            var funcName = 'tempFunc' + new Date().getTime();

            // creating temporary function and run callback in it
            window[funcName] = function(data) {
                !callback || callback(data);
            };
            //window[funcName] = null;

            var paramStr = this._getParamsString(prop.data),
                src = prop.url + '?' +
                    paramStr + '&' +
                    prop.jsonp + '=' + funcName,
                script = document.createElement('SCRIPT');
            script.src = src;
            document.body.appendChild(script);


        },

        _getParamsString: function (data) {
            var paramStr = '';
            for (var prop in data) {
                if(data.hasOwnProperty(prop)){
                    paramStr += '&' + prop + '=' + data[prop];
                }
            }
            return paramStr.slice(1);
        },

        // TEMPLATE ENGINE
        template: function (tmplFileName, data) {
            var that = this,
                html = '';
            this.ajax({
                url: tmplFileName,
                async: false
            }, function(resp) {
                if (Array.isArray(data)){
                    data.forEach(function (val) {
                        html += that._replaceInTmpl(resp, val);
                    });
                } else {
                    html += that._replaceInTmpl(resp, data);
                }
            });
            return html;
        },

        _replaceInTmpl: function (tmpl, data) {
            var html = tmpl;
            for (var prop in data) {
                if(data.hasOwnProperty(prop)){
                    if(typeof data[prop] === 'object'){
                        html = this._replaceInTmpl(html, data[prop]);
                    } else {
                        html = html.replace(new RegExp('{{' + prop + '}}','gi'), data[prop]);
                    }
                }
            }
            return html;
        }
    };

    window.G = G;
})(window);