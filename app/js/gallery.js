(function () {
    'use strict';

    var FlickrGallery = function (tag) {
        this.options = {
            url: 'https://api.flickr.com/services/feeds/photos_public.gne',
            dataType: 'jsonp',
            jsonp: 'jsoncallback',
            data: {
                format: 'json',
                tags: tag
            }
        };
        this._images = [];
        this.storage = new G.Store(this.options);
        // Gallery
        this.galleryView = new G.View({
            el: '.galleries',
            template: 'js/gallery.tmpl',
            className: tag + 'gallery'
        });
        this.galleryView.render(true);

        // Images for Gallery
        this.imageView = new G.View({
            el: '.' + tag + 'gallery',
            template: 'js/galleryImage.tmpl'
        });

    };

    FlickrGallery.prototype.getImages = function (callback) {
        var that = this;
        this.storage.fetch(function(res) {
            that._images = res.items;
            //callback();
            that.imageView.data = that._images;
            that.imageView.render();
        });
    };

    window.G = window.G || {};
    window.G.FlickrGallery = FlickrGallery;
})();