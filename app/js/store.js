(function (window, helpers) {
    'use strict';

    var Store = function (options) {
        this.options = options;
    };

    Store.prototype.fetch = function (callback) {
        helpers.ajax(this.options, callback);
    };


    window.G = window.G || {};
    window.G.Store = Store;
})(window, G.helpers);